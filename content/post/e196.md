+++
title = "E196 Ubuntu memória"
itunes_title = "Ubuntu memória"
episode = 196
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e196/e196.mp3"
podcast_duration = "1:03:44"
podcast_bytes = "91795120"
author = "Podcast Ubuntu Portugal"
date = "2022-05-26"
description = "1/3 de nós ficou estarrecido com a audição de uma melodia, algo exótica, enquanto relembrava os primórdios do Ubuntu."
thumbnail = "images/e196.png"

featured = false
categories = ["Episódio"]
tags = [
  "easypeasy",
  "eeebuntu",
  "karmic",
  "power bank",
  "launchpad",
  "ed25519",
  "release",
  "Ubuntu",
  "Collabora",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e196", "E196"]
+++

1/3 de nós ficou estarrecido com a audição de uma melodia, algo exótica, enquanto relembrava os primórdios do Ubuntu para pequenos, mas poderosos, equipamentos. Os outros 2/3 trocaram de monitores entre si... Houve ainda tempo para actualidades e para continuar a transcrição da entrevista do Mark, fiquem até ao fim!
Já sabem: oiçam, subscrevam e partilhem!

* https://iteroni.com/watch?v=RPKqM_OK9l0
* https://archiveos.org/easypeasy/
* https://archiveos.org/eeebuntu/
* https://old-releases.ubuntu.com/releases/karmic/
* https://www.discogs.com/
* https://www.chiark.greenend.org.uk/~cjwatson/blog/lp-new-ssh-features.html
* https://www.humblebundle.com/books/cybersecurity-cyber-warfare-packt-books?partner=PUP
* https://www.humblebundle.com/software/intro-to-coding-2022-software?partner=PUP
* https://www.humblebundle.com/books/coding-cookbooks-oreilly-books?partner=PUP
* https://www.humblebundle.com/software/complete-aws-training-software?partner=PUP
* https://amzn.to/3wOrm1k
* https://amzn.to/3Lz8vxA
* https://gitlab.com/podcastubuntuportugal
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

