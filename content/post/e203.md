+++
title = "E203 O Segredo de Serzedo"
itunes_title = "O Segredo de Serzedo"
episode = 203
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e203/e203.mp3"
podcast_duration = "1:01:19"
podcast_bytes = "88309341"
author = "Podcast Ubuntu Portugal"
date = "2022-07-14"
description = "O Miguel está apaixonado!"
thumbnail = "images/e203.png"

featured = false
categories = ["Episódio"]
tags = [
  "Thinkpad",
  "x260",
  "hugo",
  "traduções",
  "ansol",
  "Ubuntu",
  "Collabora",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e203", "E203"]
+++

Amanhã temos jantar do PUP! O Miguel está apaixonado! Comprou um Think e adorou! Sob o efeito desta tensão amorosa, ainda o Miguel, chamou mentiroso ao Carrondo, momento muito agradável do episódio por sinal... A ANSOL vai organizar uma série de sessões de tradução, o Diogo andou a fazer streamings secretos com o Sr. Hugo e o PUP finalmente tem a caderneta de cromos completa. Estranhamente falou-se sobre snaps, debs e outra cenas...
Já sabem: oiçam, subscrevam e partilhem!

* https://ptrefurb.pt
* https://evonomics.com/building-public-goods-21st-century/
* https://jitsi.ansol.org/traduzir
* https://matrix.to/#/#traduções:ansol.org
* https://twitter.com/m_wimpress/status/1544668552124301315
* https://github.com/wimpysworld/retro-home
* https://ubuntu.com//blog/improving-firefox-snap-performance-part-3
* https://github.com/wimpysworld/deb-get
* https://eventos.bad.pt/event/iv-jornadas-de-open-source/
* https://nextcloud.com/blog/youre-invited-nextcloud-conference-on-october-1-2-in-berlin/
* https://amzn.to/3xtq64o
* https://amzn.to/3wOrm1k
* https://amzn.to/3Lz8vxA
* https://gitlab.com/podcastubuntuportugal
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

