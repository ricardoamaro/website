+++
title = "E187 Manda-me o logo por e-mail"
itunes_title = "Manda-me o logo por e-mail"
episode = 187
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e187/e187.mp3"
podcast_duration = "0:55:39"
podcast_bytes = "53727861"
author = "Podcast Ubuntu Portugal"
date = "2022-03-24"
description = "O Miguel decidiu fazer upgrade ao seu Pi, não correu bem! O Diogo tirou o pó a um Thinkpad e o Carrondo quer repensar o seu BQ M10FHD. Analisámos o novo logotipo do Ubuntu, na nossa visão de não-designers, a nova versão do lxd sem esquecer o clássico QEMU."
thumbnail = "images/e187.png"

featured = false
categories = ["Episódio"]
tags = [
  "pi",
  "dekko",
  "thinkpad",
  "qemu",
  "logo",
  "ubuntu",
  "Collabora",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e187", "E187"]
+++

O Miguel decidiu fazer upgrade ao seu Pi, não correu bem! O Diogo tirou o pó a um Thinkpad e o Carrondo quer repensar o seu BQ M10FHD. Analisámos o novo logotipo do Ubuntu, na nossa visão de não-designers, a nova versão do lxd sem esquecer o clássico QEMU.
Já sabem: oiçam, subscrevam e partilhem!

* https://www.youtube.com/watch?v=HAmpAFO-Mcc
* https://www.youtube.com/watch?v=6O0q3rSWr8A
* https://www.youtube.com/watch?v=Blx7cdygiS8
* https://github.com/lxc/lxd/releases/tag/lxd-4.22
* https://discuss.linuxcontainers.org/t/lxd-4-22-has-been-released/13137
* https://web.archive.org/web/20220319140335/https://ubuntu.com/blog/a-new-look-for-the-circle-of-friends
* https://ubuntu.com/blog/a-new-look-for-the-circle-of-friends
* https://diff.wikimedia.org/event/wikidata-data-reuse-days-2022/
* https://web.archive.org/web/20220215011300/https://diff.wikimedia.org/event/wikidata-data-reuse-days-2022/
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

