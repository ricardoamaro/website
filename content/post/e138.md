+++
title = "E138 Radiotransmissor"
itunes_title = "Radiotransmissor"
episode = 138
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e138/e138.mp3"
podcast_duration = "0:57:00"
podcast_bytes = "55183106"
author = "Podcast Ubuntu Portugal"
date = "2021-04-15"
description = "O ritmo dos streamings do Diogo não abranda, a Canonical vai perder mais um notável mas a Keychron portou-se bem e a Canonical com a Collabora ofereceram mais prendinhas à comunidade."
thumbnail = "images/e138.png"

featured = false
categories = ["Episódio"]
tags = [
  "testes",
  "beta",
  "bug",
  "formulários",
  "forms",
  "Alan Pope",
  "Canonical",
  "Collabora",
  "Nextcloud",
  "wikidays",
  "UBPorts",
  "ubuntu",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e138", "E138"]
+++

O ritmo dos streamings do Diogo não abranda, a Canonical vai perder mais um notável mas a Keychron portou-se bem e a Canonical com a Collabora ofereceram mais prendinhas à comunidade.
Já sabem: oiçam, subscrevam e partilhem!

* https://www.twitch.tv/podcastubuntuportugal
* https://cdimages.ubuntu.com/
* http://iso.qa.ubuntu.com/
* https://formbuilder.online/
* https://www.limesurvey.org/pt
* https://apps.nextcloud.com/apps/forms
* https://twitter.com/popey/status/1380139900108963848?s=19
* https://ubuntu.com/blog/canonical-collabora-nextcloud-deliver-work-from-home-solution-to-raspberry-pi-and-enterprise-arm
* https://keychronwireless.referralcandy.com/3P2MKM7
* https://www.humblebundle.com/books/machine-learning-zero-to-hero-manning-publications-books?parner=PUP
* https://www.humblebundle.com/books/ultimate-python-bookshelf-packt-books?partner=PUP
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal



### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

