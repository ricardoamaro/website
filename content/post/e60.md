+++
title = "E60 Rumo ao Monte da Lua"
itunes_title = "Rumo ao Monte da Lua"
episode = 60
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e60/e060.mp3"
podcast_duration = "1:05:21"
podcast_bytes = "63074553"
author = "Podcast Ubuntu Portugal"
date = "2019-07-25"
description = "O drama do abandono da arquitectura de Intel a 32bits e as novidades da Ubucon Europe 2019. Sem esquecer pormenores muito pouco interessantes da vida dos intervenientes deste podcast… Já sabes, ouve, subscreve e partilha!"
thumbnail = "images/e60.png"

featured = false
categories = ["Episódio"]
tags = [
  "NOT YET",
]
seasons = ["S01"]
aliases = ["e60", "E60"]
+++

O drama do abandono da arquitectura de Intel a 32bits e as novidades da Ubucon Europe 2019. Sem esquecer pormenores muito pouco interessantes da vida dos intervenientes deste podcast… Já sabes, ouve, subscreve e partilha!

* https://discourse.ubuntu.com/t/intel-32bit-packages-on-ubuntu-from-19-10-onwards/11263/
* https://ubuntu.com/blog/statement-on-32-bit-i386-packages-for-ubuntu-19-10-and-20-04-lts
* https://ubucon.eu
* https://sintra2019.ubucon.org/call-for-papers-announcement/
* https://framaforms.org/volunteers-voluntarios-ubucon-europe-2019-sintra-1559899302


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

