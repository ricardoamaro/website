+++
title = "E79 Bansko Loves you"
itunes_title = "Bansko Loves you"
episode = 79
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e79/e079.mp3"
podcast_duration = "0:56:58"
podcast_bytes = "54817804"
author = "Podcast Ubuntu Portugal"
date = "2020-02-28"
description = "Um está em peregrinação, outro não. Um andou a ler umas coisas, o outro não. Os 2 juntos trazem esta semana toda a actualizade relacionada com os vossos temas preferidos. Já sabem: oiçam, comentem e partilhem!"
thumbnail = "images/e79.png"

featured = false
categories = ["Episódio"]
tags = [
  "lpi",
  "lvm",
  "netcat",
  "nextcloud",
  "snowboard",
  "neve",
  "hwe",
  "focal",
  "fossa",
  "budgie",
  "steam",
  "kde",
  "plasma",
  "pixelscamp",
  "lts",
  "20.04",
  "HumbleBundle",
  "LibreTrend",
  "Ubuntu",
]
seasons = ["S01"]
aliases = ["e79", "E79"]
+++

Um está em peregrinação, outro não. Um andou a ler umas coisas, o outro não. Os 2 juntos trazem esta semana toda a actualizade relacionada com os vossos temas preferidos. Já sabem: oiçam, comentem e partilhem!

* https://learning.lpi.org/en/
* https://discourse.ubuntu.com/c/server/guide
* https://lists.ubuntu.com/archives/ubuntu-announce/2020-February/000254.html
* https://discourse.ubuntu.com/t/call-for-testing-steam-in-focal-fossa/14490
* https://www.youtube.com/watch?v=AyVJUOWko98
* https://kde.org/announcements/plasma-5.18.0
* https://ubuntu.com/blog/how-to-upgrade-from-windows-7-to-ubuntu-hardware-and-software-considerations
* https://ubuntu.com/blog/how-to-upgrade-from-windows-7-to-ubuntu-installation
* https://ubuntu.com/blog/how-to-upgrade-from-windows-7-to-ubuntu-desktop-tour-and-applications
* https://open-store.io/app/onion.nanuc.org
* https://open-store.io/app/pure-maps.jonnius
* https://loco.ubuntu.com/events/ubuntu-pt/4005-hora-ubuntu-ubuntu-touch-junta-te-%C3%A0-revolu%C3%A7%C3%A3o/
* https://loco.ubuntu.com/events/ubuntu-pt/4004-encontro-ubuntu-pt-sintra/
* https://www.humblebundle.com/books/cybersecurity-2020-wiley-books?partner=PUP
* https://www.humblebundle.com/books/jumpstart-your-maker-space-make-community-books?partner=PUP
* https://www.humblebundle.com/books/user-experience-ux-design-books?partner=PUP
* https://pixels.camp/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

