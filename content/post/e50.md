+++
title = "E50 Descentralização!"
itunes_title = "Descentralização!"
episode = 50
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e50/e050.mp3"
podcast_duration = "1:09:31"
podcast_bytes = "69102356"
author = "Podcast Ubuntu Portugal"
date = "2019-03-12"
description = "Neste episódio falamos sobre as habituais noticias sobre o Universo Ubuntu e temos como tema principal o software descentralizado e o fediverso, o que é, como utilizar e quais as suas vantagens. Já sabes, ouve, subscreve e partilha!"
thumbnail = "images/e50.png"

featured = false
categories = ["Episódio"]
tags = [
  "NOT YET",
]
seasons = ["S01"]
aliases = ["e50", "E50"]
+++

Neste episódio falamos sobre as habituais noticias sobre o Universo Ubuntu e temos como tema principal o software descentralizado e o fediverso, o que é, como utilizar e quais as suas vantagens. Já sabes, ouve, subscreve e partilha!

* https://wiki.ubuntu.com/XenialXerus/ReleaseNotes/ChangeSummary/16.04.6
* https://www.omgubuntu.co.uk/2019/03/ubuntu-19-04-mascot-disco-dingo-art
* https://www.omgubuntu.co.uk/2019/03/linux-kernel-5-0-released-this-is-whats-new
* https://linuxconfig.org/how-to-install-tweak-tool-on-ubuntu-18-04-bionic-beaver-linux
* https://ubports.com/blog/ubports-blog-1/post/call-for-testing-ubuntu-touch-ota-8-206
* https://snyk.io/blog/top-ten-most-popular-docker-images-each-contain-at-least-30-vulnerabilities/
* https://docs.ubuntu.com/security-certs/en/fips-faq
* https://joinmastodon.org/
* https://diasporafoundation.org/
* https://matrix.org/blog/home/
* https://nextcloud.com/
* https://joinpeertube.org/en/
* https://disroot.org/en
* https://framasoft.org/en/
* https://blog.ubuntu.com/2019/02/26/ubuntu-eal2-certified
* https://ubuntusecuritypodcast.org/
* https://capsule8.com/blog/millions-of-binaries-later-a-look-into-linux-hardening-in-the-wild/
* https://dot.kde.org/2019/02/20/kde-adding-matrix-its-im-framework
* https://matrix.org/blog/2019/02/20/welcome-to-matrix-kde/
* https://www.sylvia-ritter.com/
* https://static1.squarespace.com/static/55d13487e4b0c8582b04b231/55dd9b84e4b0e0734874f98e/5c6c3743eef1a1909cfad011/1550595934692/DiscoDingo_SylviaRitter.jpg


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

