+++
title = "E185 Foco, mais foco!"
itunes_title = "Foco, mais foco!"
episode = 185
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e185/e185.mp3"
podcast_duration = "0:57:20"
podcast_bytes = "55345299"
author = "Podcast Ubuntu Portugal"
date = "2022-03-10"
description = "Na semana em que afinal ainda não são conhecidos os vencedores do concurso de wallpapers Jammi Jellifish o Diogo andou a melhorar a sua mestria em react, o Xubuntu anunciou o seu concurso de wallpapers e paralelamente mudou-se para para github e transifex enquanto alguns utilizadores de UBPorts podem ouvir spotify ou a sua estação de rádio preferida…"
thumbnail = "images/e185.png"

featured = false
categories = ["Episódio"]
tags = [
  "react",
  "wallpapers",
  "lxd",
  "lxc",
  "launchpad",
  "transifex",
  "fufify",
  "spotify",
  "jingpad",
  "codium",
  "ubuntu",
  "Collabora",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e185", "E185"]
+++

Na semana em que afinal ainda não são conhecidos os vencedores do concurso de wallpapers Jammi Jellifish o Diogo andou a melhorar a sua mestria em react, o Xubuntu anunciou o seu concurso de wallpapers e paralelamente mudou-se para para github e transifex enquanto alguns utilizadores de UBPorts podem ouvir spotify ou a sua estação de rádio preferida…
Já sabem: oiçam, subscrevam e partilhem!

* https://web.archive.org/web/20220307203939/https://xubuntu.org/news/xubuntu-22-04-community-wallpaper-contest/
* https://web.archive.org/web/20220307203752/https://contest.xubuntu.org/terms/
* https://web.archive.org/web/20220307204823/https://xubuntu.org/news/xubuntu-is-now-on-github/
* https://web.archive.org/web/20220128134647/https://ubottu.com/meetingology/logs/xubuntu-devel/2020/xubuntu-devel.2020-05-19-21.02.log.html#l-120
* https://github.com/Xubuntu
* https://www.transifex.com/xubuntu/public/
* https://bugs.launchpad.net/bugs/+bugs?field.searchtext=&orderby=-importance&field.status%3Alist=NEW&field.status%3Alist=CONFIRMED&field.status%3Alist=TRIAGED&field.status%3Alist=INPROGRESS&field.status%3Alist=FIXCOMMITTED&field.status%3Alist=INCOMPLETE_WITH_RESPONSE&field.status%3Alist=INCOMPLETE_WITHOUT_RESPONSE&assignee_option=any&field.assignee=&field.bug_reporter=&field.bug_commenter=&field.subscriber=&field.tag=xubuntu&field.tags_combinator=ANY&field.status_upstream-empty-marker=1&field.has_cve.used=&field.omit_dupes.used=&field.omit_dupes=on&field.affects_me.used=&field.has_patch.used=&field.has_branches.used=&field.has_branches=on&field.has_no_branches.used=&field.has_no_branches=on&field.has_blueprints.used=&field.has_blueprints=on&field.has_no_blueprints.used=&field.has_no_blueprints=on&search=Search
* https://web.archive.org/web/20220217015834/https://xubuntu.org/contribute/
* https://xubuntu.org/news/xubuntu-21-04-testing-week/
* https://web.archive.org/web/20220307204235/https://xubuntu.org/news/xubuntu-21-04-testing-week/
* https://xubuntu.org/contribute/qa/
* https://wiki.xubuntu.org/qa/new_tester_start
* https://t.me/UbuntuTesters
* https://t.me/XubuntuDevelopment
* https://twitter.com/fredldotme/status/1499517862951768066
* https://www.youtube.com/watch?v=Su1V7LeAOTY
* https://twitter.com/fredldotme/status/1500380913573806088
* https://open-store.io/app/codium.vscodium.com
* https://open-store.io/app/it.mardy.fmradio
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

