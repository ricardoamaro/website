+++
title = "E206 [ADIADO]"
itunes_title = "[ADIADO]"
episode = 206
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e206-adiado/e206-adiado.mp3"
podcast_duration = "0:00:35"
podcast_bytes = "848247"
author = "Podcast Ubuntu Portugal"
date = "2022-08-03"
description = ""
thumbnail = "images/e206-adiado.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu",
  "Collabora",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e206-adiado", "E206-adiado"]
+++

A publicação deste episódio teve de ser adiada. O episódio 206 será publicado num futuro próximo.
Já sabem: oiçam, subscrevam e partilhem!

* https://eventos.bad.pt/event/iv-jornadas-de-open-source/
* https://nextcloud.com/blog/youre-invited-nextcloud-conference-on-october-1-2-in-berlin/
* https://amzn.to/3xtq64o
* https://amzn.to/3wOrm1k
* https://amzn.to/3Lz8vxA
* https://gitlab.com/podcastubuntuportugal
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

