+++
title = "E47 Geeks aos molhos"
itunes_title = "Geeks aos molhos"
episode = 47
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e47/e047.mp3"
podcast_duration = "1:01:13"
podcast_bytes = "59670325"
author = "Podcast Ubuntu Portugal"
date = "2019-02-07"
description = "Neste episódio falamos do FOSDEM e da nossa experiência este ano. Se foste, partilha a tua experiência, se não foste cuidado, vais ficar com vontade de ir para o ano. Já sabes: Ouve, subscreve e partilha!"
thumbnail = "images/e47.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu",
  "FOSDEM",
  "Brussels",
  "Unix",
  "Jon Hall",
  "Jon maddog hall",
  "Matrix",
  "DNS over HTTP",
  "DoH",
  "DNS",
  "Containers",
  "LXD",
  "Crostini",
  "Free Software",
  "Snapcraft",
  "volunteer",
  "volunteering",
  "nextcloud",
]
seasons = ["S01"]
aliases = ["e47", "E47"]
+++

Neste episódio falamos do FOSDEM e da nossa experiência este ano. Se foste, partilha a tua experiência, se não foste cuidado, vais ficar com vontade de ir para o ano. Já sabes: Ouve, subscreve e partilha!

* https://fosdem.org/2019/
* https://fosdem.org/2019/schedule/event/keynotes_welcome/
* https://fosdem.org/2019/schedule/event/keynote_fifty_years_unix/
* https://fosdem.org/2019/schedule/event/matrix_french_state/
* https://fosdem.org/2019/schedule/event/dns_over_http/
* https://fosdem.org/2019/schedule/event/dns_privacy_panel/
* https://fosdem.org/2019/schedule/event/containers_lxd_update/
* https://fosdem.org/2019/schedule/event/crostini/
* https://fosdem.org/2019/schedule/event/full_software_freedom/
* https://fosdem.org/2019/schedule/event/behind_snapcraft/
* https://fosdem.org/2019/schedule/event/nextcloud/
* https://volunteers.fosdem.org/


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

