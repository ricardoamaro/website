+++
title = "E201 Visca Lomiri Lliure!"
itunes_title = "Visca Lomiri Lliure!"
episode = 201
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e201/e201.mp3"
podcast_duration = "1:02:43"
podcast_bytes = "90322441"
author = "Podcast Ubuntu Portugal"
date = "2022-06-30"
description = "O Miguel foi apanhado pelo inesperado DRM da RTP Play..."
thumbnail = "images/e201.png"

featured = false
categories = ["Episódio"]
tags = [
  "Ubuntu",
  "Collabora",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e201", "E201"]
+++

No rescaldo da grande festa o Miguel foi apanhado pelo inesperado DRM da RTP Play, que o impediu de continuar a seguir a série do momento, para ele... Em jeito de retaliação e/ou alívio da tensão foi programar LEDs RGB, enfim... O Diogo teve mais uma semana das suas.
Já sabem: oiçam, subscrevam e partilhem!

* https://www.rtp.pt/play/p7082/merli
* https://sequremall.com/collections/soldering-irons/products/sq-d60-soldering-iron-kit?variant=39880598257852
* https://www.direitosdigitais.pt/comunicacao/noticias/24-dia-internacional-contra-o-drm-2017-dayagainstdrm
* https://amzn.to/3xtq64o
* https://amzn.to/3wOrm1k
* https://amzn.to/3Lz8vxA
* https://gitlab.com/podcastubuntuportugal
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

