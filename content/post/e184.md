+++
title = "E184 Bola de Cristal"
itunes_title = "Bola de Cristal"
episode = 184
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e184/e184.mp3"
podcast_duration = "1:01:23"
podcast_bytes = "59235372"
author = "Podcast Ubuntu Portugal"
date = "2022-03-03"
description = "No episódio 184: dispositivos e integrações Zigbee, opções para routers abertos, robots de limpeza mais livres, mudanças no show contribuídas pela comunidade, Telegram, release do 20.04.4, eleições para a Fundação UBports, o Planet Computers Astro Slide 5G com UBports Ubuntu Touch."
thumbnail = "images/e184.png"

featured = false
categories = ["Episódio"]
tags = [
  "zigbee",
  "openwrt",
  "ubuntu",
  "Collabora",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e184", "E184"]
+++

No episódio 184: dispositivos e integrações Zigbee, opções para routers abertos, robots de limpeza mais livres, mudanças no show contribuídas pela comunidade, Telegram, release do 20.04.4, eleições para a Fundação UBports, o Planet Computers Astro Slide 5G com UBports Ubuntu Touch.
Já sabem: oiçam, subscrevam e partilhem!

* https://discourse.ubuntu.com/t/jammy-jellyfish-22-04-wallpaper-competition/26388
* https://twitter.com/ubuntu/status/1498239541559414790
* https://www.phoronix.com/scan.php?page=news_item&px=Ubuntu-20.04.4-LTS
* https://lists.ubuntu.com/archives/ubuntu-announce/2022-February/000277.html
* https://twitter.com/planetcom2017/status/1497949329675493387
* https://twitter.com/planetcom2017/status/1498239923467563008
* https://twitter.com/AyanoTDO/status/1498201019385364481
* https://twitter.com/AyanoTDO/status/1498188538189422593
* https://twitter.com/benwood/status/1498260097386131457
* https://www.www3.planetcom.co.uk/
* https://store.planetcom.co.uk/products/astro-slide?variant=41399314677954
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal


### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

