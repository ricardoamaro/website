+++
title = "E132 Rabilho, atum rabilho"
itunes_title = "Rabilho, atum rabilho"
episode = 132
podcast_file = "https://media.blubrry.com/ubuntupt/archive.org/download/pup-e132/e132.mp3"
podcast_duration = "1:00:30"
podcast_bytes = "58548959"
author = "Podcast Ubuntu Portugal"
date = "2021-03-04"
description = "Em vésperas do colapso – ou pelo menos de algumas dores de cabeça – de parte da Internet à conta dos certificados da Multicert, estivemos a olhar para cameras virtuais, nextcloud, Thunderbird, Let’s Encrypt, Ubuntu core e muito mais."
thumbnail = "images/e132.png"

featured = false
categories = ["Episódio"]
tags = [
  "Sonobus",
  "v4l2loopback",
  "raspotify",
  "birdtray",
  "nextcloud",
  "letsencrypt",
  "ssh-import-gh",
  "LoCo committee",
  "ubuntu core",
  "Mastering Ubuntu Server",
  "UBPorts",
  "ubuntu",
  "LibreTrend",
  "Slimbook",
  "Radio Zero",
  "Sr Podcast",
]
seasons = ["S01"]
aliases = ["e132", "E132"]
+++

Em vésperas do colapso – ou pelo menos de algumas dores de cabeça – de parte da Internet à conta dos certificados da Multicert, estivemos a olhar para cameras virtuais, nextcloud, Thunderbird, Let’s Encrypt, Ubuntu core e muito mais.
Já sabem: oiçam, subscrevam e partilhem!

* https://ubuntu.com//blog/ubuntu-core-20-secures-linux-for-iot
* https://discourse.ubuntu.com/t/inside-ubuntu-core-20/20777
* https://www.packtpub.com/product/mastering-ubuntu-server-third-edition/9781800564640
* https://open-store.io/app/teleports.ubports
* https://ubports.com/blog/ubport-blogs-news-1/post/the-ubuntu-touch-devices-website-has-leveled-up-3740
* https://wiki.mozilla.org/CA:Camerfirma_Issues
* https://www.humblebundle.com/books/pocket-reference-guides-oreilly-books?partnet=PUP
* http://keychronwireless.refr.cc/tiagocarrondo
* https://shop.nitrokey.com/shop/product/nk-pro-2-nitrokey-pro-2-3?aff_ref=3
* https://shop.nitrokey.com/shop?aff_ref=3
* https://youtube.com/PodcastUbuntuPortugal



### Apoios
Podem apoiar o podcast usando os links de afiliados do Humble Bundle, porque ao usarem esses links para fazer uma compra, uma parte do valor que pagam reverte a favor do Podcast Ubuntu Portugal.
E podem obter tudo isso com 15 dólares ou diferentes partes dependendo de pagarem 1, ou 8.
Achamos que isto vale bem mais do que 15 dólares, pelo que se puderem paguem mais um pouco mais visto que têm a opção de pagar o quanto quiserem.
Se estiverem interessados em outros bundles não listados nas notas usem o link https://www.humblebundle.com/?partner=PUP e vão estar também a apoiar-nos.

### Atribuição e licenças
Este episódio foi produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).
O website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).
A música do genérico é: "Won't see it comin' (Feat Aequality & N'sorte d'autruche)", por Alpha Hydrae e está licenciada nos termos da [CC0 1.0 Universal License](https://creativecommons.org/publicdomain/zero/1.0/).
Este episódio e a imagem utilizada estão licenciados nos termos da licença: [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/), [cujo texto integral pode ser lido aqui](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode). Estamos abertos a licenciar para permitir outros tipos de utilização, [contactem-nos](https://podcastubuntuportugal.org/contactos) para validação e autorização.

