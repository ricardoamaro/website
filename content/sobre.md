+++
title = "Sobre o Podcast Ubuntu Portugal"
description = "O podcast em português sobre Ubuntu e a sua comunidade."
date = "2017-06-12"
aliases = ["about-us", "about", "contact"]
author = "Podcast Ubuntu Portugal"
+++

![Podcast Ubuntu Portugal](../images/thumbnail-256.png)

Um podcast descontraído sobre Ubuntu, a comunidade Ubuntu e tudo o que gira em volta do universo Ubuntu.

## Autoria

### &rarr; Diogo Constantino


### &rarr; Miguel

[telegram](https://t.me/per_sonne) |
[twitter](https://twitter.com/personne_tweets/) |

### &rarr; Tiago Carrondo

[telegram](https://t.me/tcarrondo) |
[linkedin](https://www.linkedin.com/in/carrondo) |
[github](https://github.com/tcarrondo/) |
[gitlab](https://gitlab.com/tcarrondo/) |
[twitter](https://twitter.com/tcarrondo/) |
[facebook](http://facebook.com/paginacarrondo) |
[instagram](http://instagram.com/paginacarrondo)

### &rarr; David Negreira (em Sabática)


## Atribuição e licenças

Este projecto é produzido por Diogo Constantino, Miguel e Tiago Carrondo e editado pelo [Senhor Podcast](https://senhorpodcast.pt/).

Este website é produzido por Tiago Carrondo e o [código aberto](https://gitlab.com/podcastubuntuportugal/website) está licenciado nos termos da [Licença MIT](https://gitlab.com/podcastubuntuportugal/website/main/LICENSE).

A música do genérico é: "Won’t see it comin’ (Feat Aequality & N’sorte d’autruche)", por Alpha Hydrae e está licenciada nos termos da [Licença Creative Commons - CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

As imagens usadas estão licenciadas nos termos da [Licença Creative Commons - Attribution-NonCommercial-NoDerivatives 4.0 International](https://creativecommons.org/licenses/by-nc-nd/4.0/).
